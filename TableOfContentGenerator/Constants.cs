﻿namespace TableOfContentGenerator
{
    public static class Constants
    {
        public const string ChapterRegex = @"Chương \w+";
        public const string SectionRegex = @"MỤC \d+\.";
        public const string ArticleRegex = @"Điều \d+\.";
    }
}
